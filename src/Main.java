import schedulingAlgorithms.*;
import schedulingAlgorithms.utils.Process;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by macbookpro on 09.03.2016.
 */
public class Main {
    public static void main(String[] args) {

        task1();
        task2();
        task3();
    }

    private static void task3() {
        List<Process> listOfProcesses = initProcessesTask3();

        int averageProcessTime = 0;
        int max = 0;

        for (Process process : listOfProcesses) {
            averageProcessTime += process.getEntryTime();
            if (process.getEntryTime() > max) {
                max = process.getEntryTime();
            }
        }

        averageProcessTime /= listOfProcesses.size();

        double FCFS = new FirstComeFirstServed()
                .addProcessesForSimulation(copyListOfProcesses(listOfProcesses))
                .startSimulation()
                .getAvarageAwaitingTime();

        double SJF = new ShortestJobFirst()
                .addProcessesForSimulation(copyListOfProcesses(listOfProcesses))
                .startSimulation()
                .getAvarageAwaitingTime();

        double SJFPreemptive = new ShortestJobFirstPreemptive()
                .addProcessesForSimulation(copyListOfProcesses(listOfProcesses))
                .startSimulation()
                .getAvarageAwaitingTime();

        double RR5 = new RoundRobin()
                .addProcessesForSimulation(copyListOfProcesses(listOfProcesses))
                .setQuantumTime((int) (0.1 * averageProcessTime))
                .startSimulation()
                .getAverageAwaitingTime();

        double RR10 = new RoundRobin()
                .addProcessesForSimulation(copyListOfProcesses(listOfProcesses))
                .setQuantumTime((int) (0.2 * averageProcessTime))
                .startSimulation()
                .getAverageAwaitingTime();

        double RR20 = new RoundRobin()
                .addProcessesForSimulation(copyListOfProcesses(listOfProcesses))
                .setQuantumTime((int) (0.4 * averageProcessTime))
                .startSimulation()
                .getAverageAwaitingTime();

        double RR40 = new RoundRobin()
                .addProcessesForSimulation(copyListOfProcesses(listOfProcesses))
                .setQuantumTime((int) (0.6 * averageProcessTime))
                .startSimulation()
                .getAverageAwaitingTime();

        double RR80 = new RoundRobin()
                .addProcessesForSimulation(copyListOfProcesses(listOfProcesses))
                .setQuantumTime((int) (0.8 * averageProcessTime))
                .startSimulation()
                .getAverageAwaitingTime();

        double RR100 = new RoundRobin()
                .addProcessesForSimulation(copyListOfProcesses(listOfProcesses))
                .setQuantumTime(max - (int) Math.sqrt(averageProcessTime))
                .startSimulation()
                .getAverageAwaitingTime();

        System.out.println("FCFS average awaiting time: " + FCFS);
        System.out.println("SJF average awaiting time: " + SJF);
        System.out.println("SJF Preemptive average awaiting time: " + SJFPreemptive);
        System.out.println("RR with quantum = 5 average awaiting time: " + RR5);
        System.out.println("RR with quantum = 10 average awaiting time: " + RR10);
        System.out.println("RR with quantum = 20 average awaiting time: " + RR20);
        System.out.println("RR with quantum = 40 average awaiting time: " + RR40);
        System.out.println("RR with quantum = 80 average awaiting time: " + RR80);
        System.out.println("RR with quantum = 100 average awaiting time: " + RR100);


    }

    private static void task2() {

        List<Process> RRResults = new RoundRobin()
                .addProcessesForSimulation(initProcessesTask2())
                .sortProcessesByPriority()
                .setQuantumTime(2)
                .startSimulation()
                .getResults();

        List<Process> SJFResults = new ShortestJobFirst()
                .addProcessesForSimulation(initProcessesTask2())
                .startSimulation()
                .getResults();

        List<Process> SJFRPriorityResults = new ShortestJobFirstByPriority()
                .addProcessesForSimulation(initProcessesTask2())
                .startSimulation()
                .getResults();

    }

    public static void task1() {
        List<Process> FCFSResults = new FirstComeFirstServed()
                .addProcessesForSimulation(initProcessesTask1())
                .startSimulation()
                .getResults();

        List<Process> SJFResults = new ShortestJobFirst()
                .addProcessesForSimulation(initProcessesTask1())
                .startSimulation()
                .getResults();
    }

    public static List<Process> initProcessesTask1() {
        List<Process> listOfProcesses = new ArrayList<>();

        Process process1 = new Process()
                .setId(1)
                .setEntryTime(0)
                .setServiceTime(8);

        Process process2 = new Process()
                .setId(2)
                .setEntryTime(2)
                .setServiceTime(5);

        Process process3 = new Process()
                .setId(3)
                .setEntryTime(3)
                .setServiceTime(3);

        Process process4 = new Process()
                .setId(4)
                .setEntryTime(9)
                .setServiceTime(4);

        listOfProcesses.add(process1);
        listOfProcesses.add(process3);
        listOfProcesses.add(process4);
        listOfProcesses.add(process2);

        return listOfProcesses;
    }

    public static List<Process> initProcessesTask2() {
        List<Process> listOfProcesses = new ArrayList<>();

        Process process1 = new Process()
                .setId(1)
                .setEntryTime(0)
                .setServiceTime(10)
                .setPriority(3);

        Process process2 = new Process()
                .setId(2)
                .setEntryTime(0)
                .setServiceTime(5)
                .setPriority(5);

        Process process3 = new Process()
                .setId(3)
                .setEntryTime(0)
                .setServiceTime(2)
                .setPriority(2);

        Process process4 = new Process()
                .setId(4)
                .setEntryTime(0)
                .setServiceTime(4)
                .setPriority(1);

        Process process5 = new Process()
                .setId(5)
                .setEntryTime(0)
                .setServiceTime(8)
                .setPriority(4);

        listOfProcesses.add(process1);
        listOfProcesses.add(process3);
        listOfProcesses.add(process4);
        listOfProcesses.add(process2);
        listOfProcesses.add(process5);


        return listOfProcesses;
    }

    //    80 z zakresu od 1 do 10
    //    20 z zakresu od 100 do 100
    // losowanie w zakresie wybierania tych samych procesow
    // dla struktury wywlaszczeniowej dodatkowa kolejka

    public static List<Process> initProcessesTask3() {
        List<Process> listOfProcesses = new ArrayList<>();

        final int quantityOfProcessesInSequence = 100;

        final int quantityOfShortProcessesInSequence = 80;
        final int quantityOfLongProcessesInSequence = 20;

        final int maxLongProcessLength = 1000;
        final int minLongProcessLength = 100;

        final int maxShortProcessLength = 10;
        final int minShortProcessLength = 1;

        int lengthOfProcess;
        int randomTime;

        for (int i = 0; i < quantityOfShortProcessesInSequence; i++) {
            lengthOfProcess = minShortProcessLength + (int) (Math.random() * (maxShortProcessLength - minShortProcessLength + 1));
            randomTime = (int) (Math.random() * (maxShortProcessLength));

            Process process = new Process()
                    .setId(i)
                    .setEntryTime(randomTime)
                    .setServiceTime(lengthOfProcess);

            listOfProcesses.add(process);
        }


        for (int i = 0; i < quantityOfLongProcessesInSequence; i++) {
            lengthOfProcess = minLongProcessLength + (int) (Math.random() * (maxLongProcessLength - minLongProcessLength + 1));
            randomTime = (int) (Math.random() * (maxLongProcessLength));

            Process process = new Process()
                    .setId(i)
                    .setEntryTime(randomTime)
                    .setServiceTime(lengthOfProcess);

            listOfProcesses.add(process);
        }

        return listOfProcesses;
    }

    public static List<Process> copyListOfProcesses(List<Process> list) {
        ArrayList<Process> copiedList = new ArrayList<>();
        for (Process process : list) {
            Process processToCopy = new Process(process);
            copiedList.add(processToCopy);
        }

        return copiedList;
    }
}
