package schedulingAlgorithms.utils;

/**
 * Created by macbookpro on 09.03.2016.
 */
public class Process {
    private int id;
    private int entryTime;
    private int serviceTime;
    private int serviceTimeRemaining;
    private int awaitingTime;
    private int priority;

    public Process() {
    }

    public Process(Process anotherProcess)
    {
        this.id = anotherProcess.getId();
        this.entryTime = anotherProcess.getEntryTime();
        this.serviceTime = anotherProcess.getServiceTime();
        this.serviceTimeRemaining = anotherProcess.getServiceTimeRemaining();
        this.awaitingTime = anotherProcess.getAwaitingTime();
        this.priority = anotherProcess.getPriority();
    }

    public int getId() {
        return id;
    }

    public Process setId(int id) {
        this.id = id;
        return this;
    }

    public int getEntryTime() {
        return entryTime;
    }

    public Process setEntryTime(int entryTime) {
        this.entryTime = entryTime;
        return this;
    }

    public int getServiceTime() {
        return serviceTime;
    }

    public Process setServiceTime(int serviceTime) {
        this.serviceTime = serviceTime;
        this.serviceTimeRemaining = serviceTime;
        return this;
    }

    public int getAwaitingTime() {
        return awaitingTime;
    }

    public Process setAwaitingTime(int awaitingTime) {
        this.awaitingTime = awaitingTime;
        return this;
    }

    public int getPriority() {
        return priority;
    }

    public Process setPriority(int priority) {
        this.priority = priority;
        return this;
    }

    public int getServiceTimeRemaining() {
        return serviceTimeRemaining;
    }

    public Process setServiceTimeRemaining(int serviceTimeRemaining) {
        this.serviceTimeRemaining = serviceTimeRemaining;
        return this;
    }

    @Override
    protected Process clone() throws CloneNotSupportedException {
        return new Process()
                .setId(id)
                .setEntryTime(entryTime)
                .setServiceTime(serviceTime)
                .setServiceTimeRemaining(serviceTimeRemaining);
    }
}
