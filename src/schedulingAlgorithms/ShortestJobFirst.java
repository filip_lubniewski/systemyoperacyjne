package schedulingAlgorithms;

import schedulingAlgorithms.utils.Process;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by macbookpro on 09.03.2016.
 */
public class ShortestJobFirst {
    private List<Process> listOfProcesses = new ArrayList<>();
    private List<Process> resultListOfProcesses = new ArrayList<>();

    private int currentTime;

    public ShortestJobFirst() {
    }

    public ShortestJobFirst addProcessesForSimulation(List<Process> listOfProcesses) {
        this.listOfProcesses = listOfProcesses;
        return this;
    }

    public ShortestJobFirst startSimulation() {
        currentTime = 0;
        sortProcessesByServiceTime();
        processOneByOne();
        return this;
    }

    private void processOneByOne() {

        while (!listOfProcesses.isEmpty()) {
            findNewJobToProcess();
        }
    }

    private void findNewJobToProcess() {
        boolean foundJobToProcess = false;

        for (int i = 0; i < listOfProcesses.size() && !foundJobToProcess; i++) {
            if (listOfProcesses.get(i).getEntryTime() <= currentTime) {
                foundJobToProcess = true;
                processJob(i);
            }
        }

        if (!foundJobToProcess) {
            currentTime++;
        }
    }

    private void processJob(int i) {
        Process jobToProcess = listOfProcesses.get(i);

        currentTime += jobToProcess.getServiceTime();
        jobToProcess.setAwaitingTime(currentTime - (jobToProcess.getEntryTime() + jobToProcess.getServiceTime()));

        listOfProcesses.remove(i);
        resultListOfProcesses.add(jobToProcess);
    }

    private void sortProcessesByServiceTime() {
        Collections.sort(listOfProcesses, (o1, o2) ->
                (o1.getServiceTime() > o2.getServiceTime()) ? 1 :
                        (o1.getServiceTime() < o2.getServiceTime()) ? -1 : 0);
    }

    public List<Process> getResults() {
        return resultListOfProcesses;
    }

    public double getAvarageAwaitingTime() {
        double averageAwaitingTime = 0;

        for (Process process : resultListOfProcesses) {
            averageAwaitingTime += (double) process.getAwaitingTime();
        }

        return averageAwaitingTime / resultListOfProcesses.size();
    }
}
