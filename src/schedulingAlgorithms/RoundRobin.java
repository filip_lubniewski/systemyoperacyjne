package schedulingAlgorithms;

import schedulingAlgorithms.utils.Process;

import java.util.*;

/**
 * Created by macbookpro on 09.03.2016.
 */
public class RoundRobin {
    private List<Process> listOfProcesses = new ArrayList<>();
    private List<Process> queueOfProcesses = new ArrayList<>();
    private List<Process> resultListOfProcesses = new ArrayList<>();
    private int currentTime;
    private int quantumTime;

    public RoundRobin() {
    }

    public RoundRobin addProcessesForSimulation(List<Process> listOfProcesses) {
        this.listOfProcesses = listOfProcesses;

        return this;
    }

    public RoundRobin setQuantumTime(int i) {
        this.quantumTime = i;

        return this;
    }

    public RoundRobin startSimulation() {
        currentTime = 0;
        sortProcessesByEntryTime();
        processOneByOne();

        return this;
    }

    private void processOneByOne() {
        Process currentProcess;

        if (!listOfProcesses.isEmpty()) {
            queueOfProcesses.add(listOfProcesses.get(0));
            listOfProcesses.remove(0);

            while (!queueOfProcesses.isEmpty()) {
                currentProcess = queueOfProcesses.get(0);
                queueOfProcesses.remove(0);

                currentTime += Math.min(quantumTime, currentProcess.getServiceTimeRemaining());
                addEligibleProcessesToQueue();

                if (currentProcess.getServiceTimeRemaining() > quantumTime) {
                    currentProcess.setServiceTimeRemaining(currentProcess.getServiceTimeRemaining() - quantumTime);
                    queueOfProcesses.add(currentProcess);
                } else {
                    currentProcess.setAwaitingTime(currentTime - (currentProcess.getEntryTime() + currentProcess.getServiceTime()));
                    currentProcess.setServiceTimeRemaining(0);
                    resultListOfProcesses.add(currentProcess);
                }
            }
        }
    }

    private void addEligibleProcessesToQueue() {

        Iterator<Process> it = listOfProcesses.iterator();
        while (it.hasNext()) {
            Process process = it.next();
            if (process.getEntryTime() <= currentTime) {
                queueOfProcesses.add(process);
                it.remove();
            }
        }

    }

    private void sortProcessesByEntryTime() {
        Collections.sort(listOfProcesses, (o1, o2) ->
                (o1.getEntryTime() > o2.getEntryTime()) ? 1 :
                        (o1.getEntryTime() < o2.getEntryTime()) ? -1 : 0);
    }

    public RoundRobin sortProcessesByPriority() {
        Collections.sort(listOfProcesses, (o1, o2) ->
                (o1.getPriority() < o2.getPriority()) ? 1 :
                        (o1.getPriority() > o2.getPriority()) ? -1 :
                                (o1.getEntryTime() > o2.getEntryTime()) ? 1 :
                                        (o1.getEntryTime() < o2.getEntryTime()) ? -1 : 0);

        return this;
    }

    public List<Process> getResults() {
        return resultListOfProcesses;
    }

    public double getAverageAwaitingTime() {
        double averageAwaitingTime = 0;

        for (Process process : resultListOfProcesses) {
            averageAwaitingTime += (double) process.getAwaitingTime();
        }

        return averageAwaitingTime / resultListOfProcesses.size();
    }
}
