package schedulingAlgorithms;

import schedulingAlgorithms.utils.Process;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by macbookpro on 09.03.2016.
 */
public class FirstComeFirstServed {
    private List<Process> listOfProcesses = new ArrayList<>();
    private int currentTime;

    public FirstComeFirstServed() {
    }

    public FirstComeFirstServed addProcessesForSimulation(List<Process> listOfProcesses) {
        this.listOfProcesses = listOfProcesses;
        return this;
    }

    public FirstComeFirstServed startSimulation() {
        currentTime = 0;
        sortProcessesByEntryTime();
        processOneByOne();
        return this;
    }

    private void processOneByOne() {
        for (Process currentProcess : listOfProcesses) {
            currentTime += currentProcess.getServiceTime();
            currentProcess.setAwaitingTime(currentTime - (currentProcess.getEntryTime() + currentProcess.getServiceTime()));
        }
    }

    private void sortProcessesByEntryTime() {
        Collections.sort(listOfProcesses, (o1, o2) ->
                (o1.getEntryTime() > o2.getEntryTime()) ? 1 :
                        (o1.getEntryTime() < o2.getEntryTime()) ? -1 : 0);
    }

    public List<Process> getResults() {
        return listOfProcesses;
    }

    public double getAvarageAwaitingTime() {
        double averageAwaitingTime = 0;

        for (Process process : listOfProcesses) {
            averageAwaitingTime += (double) process.getAwaitingTime();
        }

        return averageAwaitingTime / listOfProcesses.size();
    }
}
