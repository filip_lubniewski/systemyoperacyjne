package schedulingAlgorithms;

import schedulingAlgorithms.utils.Process;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

/**
 * Created by macbookpro on 09.03.2016.
 */
public class ShortestJobFirstPreemptive {
    private List<Process> listOfProcesses = new ArrayList<>();
    private List<Process> listOfEligibleProcesses = new ArrayList<>();
    private List<Process> resultListOfProcesses = new ArrayList<>();

    private int currentTime;

    public ShortestJobFirstPreemptive() {
    }

    public ShortestJobFirstPreemptive addProcessesForSimulation(List<Process> listOfProcesses) {
        this.listOfProcesses = listOfProcesses;
        return this;
    }

    public ShortestJobFirstPreemptive startSimulation() {
        currentTime = 0;
        sortProcessesByEntryTime();
        processOneByOne();
        return this;
    }

    private void processOneByOne() {
        if (!listOfProcesses.isEmpty()) {
            Process currentProcess;
            currentTime = listOfProcesses.get(0).getEntryTime();

            while (!listOfProcesses.isEmpty() || !listOfEligibleProcesses.isEmpty()) {
                addEligibleProcesses();
                sortEligibleProcessesByServiceTimeRemaining();

                currentProcess = listOfEligibleProcesses.get(0);
                currentProcess.setServiceTimeRemaining(currentProcess.getServiceTimeRemaining() - 1);

                currentTime++;

                if (currentProcess.getServiceTimeRemaining() == 0) {
                    listOfEligibleProcesses.remove(currentProcess);
                    resultListOfProcesses.add(currentProcess);
                    currentProcess.setAwaitingTime(currentTime - (currentProcess.getEntryTime() + currentProcess.getServiceTime()));
                }
            }
        }
    }

    private void addEligibleProcesses() {
        Iterator<Process> it = listOfProcesses.iterator();
        while (it.hasNext()) {
            Process process = it.next();
            if (process.getEntryTime() <= currentTime) {
                listOfEligibleProcesses.add(process);
                it.remove();
            }
        }
    }

    private void sortProcessesByEntryTime() {
        Collections.sort(listOfProcesses, (o1, o2) ->
                (o1.getEntryTime() > o2.getEntryTime()) ? 1 :
                        (o1.getEntryTime() < o2.getEntryTime()) ? -1 :
                                (o1.getServiceTime() > o2.getServiceTime()) ? 1 :
                                        (o1.getServiceTime() < o2.getServiceTime()) ? -1 : 0);
    }

    private void sortEligibleProcessesByServiceTimeRemaining() {
        Collections.sort(listOfEligibleProcesses, (o1, o2) ->
                (o1.getServiceTimeRemaining() > o2.getServiceTimeRemaining()) ? 1 :
                        (o1.getServiceTimeRemaining() < o2.getServiceTimeRemaining()) ? -1 :
                                (o1.getEntryTime() > o2.getEntryTime()) ? 1 :
                                        (o1.getEntryTime() < o2.getEntryTime()) ? -1 : 0);
    }

    public List<Process> getResults() {
        return resultListOfProcesses;
    }

    public double getAvarageAwaitingTime() {
        double averageAwaitingTime = 0;

        for (Process process : resultListOfProcesses) {
            averageAwaitingTime += (double) process.getAwaitingTime();
        }

        return averageAwaitingTime / resultListOfProcesses.size();
    }
}
